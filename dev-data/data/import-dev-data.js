const fs = require('fs')
const mongoose = require('mongoose');
const Item = require('../../model/item');
const catchAsync = require('../errors/catchAsync');

mongoose.connect(_db, {useNewUrlParser: true, useUnifiedTopology: true})
    .then((result) => console.log('connected'))
    .catch((error) => console.log(error));

// READ JSON FILE

const items = JSON.parse(fs.readFileSync('../../file/jsonfile.json', 'utf-8'));

// IMPORT DATA INTO DATABASE
const importData = catchAsync(async () => {
    await Item.create(items);
    console.log('Success')
});

//DELETE
const deleteData = catchAsync(async () => {
    await Item.deleteMany(items);
    console.log('Deleted')
});

if(process.argv[2] === '--import') {
    importData();
} else if (process.argv[2] === '--delete'){
    deleteData();
}

