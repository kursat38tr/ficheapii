const catchAsync = require('../errors/catchAsync');
const Item = require('../model/item');
// const Main = require("../model/main");
// const fs = require("fs");
// const Main = require("../model/main");

// const item = JSON.parse(
//     fs.readFileSync(`${__dirname}/../file/jsonfile.json`)
// );

exports.getAllItems = catchAsync(async (req, res) => {
    const getItems = await Item.find();
    res.status(200).json({
        status: 'success',
        results: getItems.length,
        data: {
            getItems
        }
    });
});


exports.createItem = catchAsync(async (req, res, next) => {

    let newItem;

    Item.countDocuments({itemId: req.body.itemId}, catchAsync( async function (err, count) {
        if (count > 0) {
            newItem = await Item.findOneAndUpdate({itemId: req.body.itemId}, req.body, {
                new: true,
                runValidators: true
            });
        } else {
            newItem = await Item.create(req.body);
        }
        res.status(200).json({
            status: 'success',
            data: {
                item: newItem
            }
        })
    }));
});

exports.updateItem = catchAsync(async (req, res) => {

    const item = await Item.update(req.body, {
        new: true,
        runValidators: true
    });

    res.status(200).json({
        status: 'success',
        data: {
            item
        }
    });
});
