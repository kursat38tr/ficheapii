const mongoose = require('mongoose');

const salesPriceSchema = new mongoose.Schema({
    id: {
        type: Number,
        required: [true, 'Please tell us the id']
    },
    itemPricingAndQuantity: {
        type: Number,
        required: [true, 'Please tell us the id']
    },
    priceCluster: {
        type: Number,
        required: [true, 'Please tell us the id']
    },
    countryCode: {
        type: String,
        required: [true, 'Please tell us the id']
    },
    priceInclVat: {
        type: Number,
        required: [true, 'Please tell us the id']
    },
    priceExclVat: {
        type: Number,
        required: [true, 'Please tell us the id']
    },
    vatType: {
        type: String,
        required: [true, 'Please tell us the id']
    },
    vatAmount: {
        type: Number,
        required: [true, 'Please tell us the id']
    },
    margin: {
        type: Number,
        required: [true, 'Please tell us the id']
    },
    validFrom: {
        type: Date,
        required: [true, 'Please tell us the id']
    },
    validTo: {
        type: Date,
        required: [true, 'Please tell us the id']
    },
});

const salesPrice = mongoose.model('salesPrice', salesPriceSchema);

module.exports = {
    salesPrice,
    salesPriceSchema
}