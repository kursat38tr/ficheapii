const mongoose = require('mongoose');

const itemLineageNodeSchema = new mongoose.Schema({
    id: {
        type: Number,
        required: [true, 'Please tell us the id']
    },
    type: {
        type: String,
        required: [true, 'Please tell us the id']
    }
});

const itemLineageNode = mongoose.model('itemLineageNode', itemLineageNodeSchema);

module.exports = {
    itemLineageNode,
    itemLineageNodeSchema
}