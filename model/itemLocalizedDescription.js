const mongoose = require('mongoose');

const itemLocalizedDescriptionSchema = new mongoose.Schema({
    id: {
        type: Number,
        required: [true, 'Please tell us the id']
    },
    itemId: {
        type: Number,
        required: [true, 'Please tell us the id']
    },
    language: {
        type: String,
        required: [true, 'Please tell us the id']
    },
    type: {
        type: String,
        required: [true, 'Please tell us the id']
    },
    description: {
        type: String,
        required: [true, 'Please tell us the id']
    }
});

const itemLocalizedDescription = mongoose.model('itemLocalizedDescription', itemLocalizedDescriptionSchema);

module.exports = {
    itemLocalizedDescription,
    itemLocalizedDescriptionSchema
}