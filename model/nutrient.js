const mongoose = require('mongoose');

const nutrientSchema = new mongoose.Schema({
    itemNutrientsId: {
        type: Number,
        required: [true, 'Please tell us the id']
    },
    itemNutrientsItemId: {
        type: Number,
        required: [true, 'Please tell us the id']
    },
    itemNutrientsCode: {
        type: String,
        required: [true, 'Please tell us the id']
    },
    itemNutrientsName: {
        type: String,
        required: [true, 'Please tell us the id']
    },
    itemNutrientsAsSold: {
        type: Number,
        required: [true, 'Please tell us the id']
    },
    itemNutrientsValuePerPortion: {
        type: Number,
        required: [true, 'Please tell us the id']
    },
});

const nutrient = mongoose.model('itemHACCPS', nutrientSchema);

module.exports = {
    nutrient,
    nutrientSchema
}