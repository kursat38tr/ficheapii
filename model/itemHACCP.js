const mongoose = require('mongoose');

const itemHACCPSchema = new mongoose.Schema({
    id: {
        type: Number,
        required: [true, 'Please tell us the id']
    },
    itemId: {
        type: Number,
        required: [true, 'Please tell us the id']
    },
    type: {
        type: String,
        required: [true, 'Please tell us the id']
    },
    code: {
        type: String,
        required: [true, 'Please tell us the id']
    },
    description: {
        type: String,
        required: [true, 'Please tell us the id']
    },
    addition: {
        type: String,
        required: [true, 'Please tell us the id']
    },
});

const itemHACCPS = mongoose.model('itemHACCPS', itemHACCPSchema);

module.exports = {
    itemHACCPS,
    itemHACCPSchema
}