const mongoose = require('mongoose');
const {locationSchema} = require("./location");

const recipeSetLocationSchema = new mongoose.Schema({
    location: [locationSchema]
});

const recipeSetLocation = mongoose.model('recipeSetLocation', recipeSetLocationSchema);

module.exports = {
    recipeSetLocation,
    recipeSetLocationSchema
}