const mongoose = require('mongoose');

const itemEANSchema = new mongoose.Schema({
    id: {
        type: Number,
        required: [true, 'Please tell us the id']
    },
    type: {
        type: String,
        required: [true, 'Please tell us the id']
    },
    EAN: {
        type: String,
        required: [true, 'Please tell us the id']
    }
});

const itemEAN = mongoose.model('itemEAN', itemEANSchema);

module.exports = {
    itemEAN,
    itemEANSchema
}