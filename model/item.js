const mongoose = require('mongoose');
const {Main, mainSchema} = require("../model/main");
const {assortmentSchema} = require("./assortment");
const {salesDataSchema} = require("./salesData");
const {statusSchema} = require("./status");
const {ingredientItemSchema} = require("./ingredientItem");

const itemSchema = new mongoose.Schema({
    itemId: {
        type: Number,
        required: [true, 'Please tell us the product name!']
    },
    main: mainSchema,
    salesData: salesDataSchema,
    assortment: assortmentSchema,
    ingredientItem: [ingredientItemSchema],
    status: [statusSchema]
});

const Item = mongoose.model('item', itemSchema);

module.exports = Item;