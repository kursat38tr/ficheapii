const mongoose = require('mongoose');

const locationSchema = new mongoose.Schema({
    id: {
        type: Number,
        required: [true, 'Please tell us the id']
    }
});

const location = mongoose.model('location', locationSchema);

module.exports = {
    location,
    locationSchema
}