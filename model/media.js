const mongoose = require('mongoose');

const mediaSchema = new mongoose.Schema({
    attachment: {
        type: String,
        required: [true, 'Please tell us the product name!']
    },
});

const Media = mongoose.model('media', mediaSchema);

module.exports = {
    Media,
    mediaSchema
};