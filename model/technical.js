const mongoose = require('mongoose');

const technicalSchema = new mongoose.Schema({
    orderNo: {
        type: Number,
        required: [true, 'Please tell us the id']
    }
});

const technical = mongoose.model('technical', technicalSchema);

module.exports = {
    technical,
    technicalSchema
}