const mongoose = require('mongoose');
const {preparationDescriptionTranslationSchema} = require("./preparationDescriptionTranslation");
const {mediaSchema} = require("./media");

const preparationSchema = new mongoose.Schema({
    preparationId: {
        type: Number,
        required: [true, 'Please tell us the id']
    },
    itemId: {
        type: Number,
        required: [true, 'Please tell us the id']
    },
    preparationType: {
        type: String,
        required: [true, 'Please tell us the id']
    },
    preparationStep: {
        type: Number,
        required: [true, 'Please tell us the id']
    },
    preparationLanguage: {
        type: String,
        required: [true, 'Please tell us the id']
    },
    preparationDescription: {
        type: String,
        required: [true, 'Please tell us the id']
    },
    preparationDescriptionTranslation: [preparationDescriptionTranslationSchema],
    media: [mediaSchema]
});

const preparation = mongoose.model('preparation', preparationSchema);

module.exports = {
    preparation,
    preparationSchema
}