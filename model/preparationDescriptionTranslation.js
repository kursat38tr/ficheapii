const mongoose = require('mongoose');
const {salesPriceSchema} = require("./salesPrice");

const preparationDescriptionTranslationSchema = new mongoose.Schema({
    language: {
        type: String,
        required: [true, 'Please tell us the id']
    },
    text: {
        type: String,
        required: [true, 'Please tell us the id']
    },
});

const preparationDescriptionTranslation = mongoose.model('preparationDescriptionTranslation', preparationDescriptionTranslationSchema);

module.exports = {
    preparationDescriptionTranslation,
    preparationDescriptionTranslationSchema
}