const mongoose = require('mongoose');

const ingredientSchema = new mongoose.Schema({
    itemIngredientsId: {
        type: Number,
        required: [true, 'Please tell us the id']
    },
    itemIngredientsItemId: {
        type: Number,
        required: [true, 'Please tell us the id']
    },
    itemIngredientsDeclarationId: {
        type: Number,
        required: [true, 'Please tell us the id']
    },
    itemIngredientsDeclaration: {
        type: String,
        required: [true, 'Please tell us the id']
    },
});

const ingredient = mongoose.model('ingredient', ingredientSchema);

module.exports = {
    ingredient,
    ingredientSchema
}