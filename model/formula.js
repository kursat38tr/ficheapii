const mongoose = require('mongoose');

const formulaSchema = new mongoose.Schema({
    location: {
        type: Number,
        required: [true, 'Please tell us the id']
    }
});

const formula = mongoose.model('formula', formulaSchema);

module.exports = {
    formula,
    formulaSchema
}