const mongoose = require('mongoose');
const {Status, statusSchema} = require('../model/status')
const {itemDescriptionTranslation, itemDescriptionTranslationSchema} = require('../model/itemDescriptionTranslation')
const {Media, mediaSchema} = require('../model/media');

const mainSchema = new mongoose.Schema({
    itemId: {
        type: Number,
        required: [true, 'Please tell us the product name!']
    },
    itemDescription: {
        type: String,
        required: [true, 'Please tell us the product name!']
    },
    preference: {
        type: String,
        // required: [true, 'Please tell us the product name!']
    },
    itemType: {
        type: String,

    },
    status : [statusSchema],
    franchiseOnlyItem: {
        type: String,

    },
    itemNumber: {
        type: Number,

    },
    itemDescriptionTranslation: [itemDescriptionTranslationSchema],
    category: {
        type: String,
    },
    media: [mediaSchema]
});

//EMBEDDED
// mainSchema.pre('save', async function (next){
//     const statusPromise = this.status.map(async id => await Status.findById(id));
//     this.status = await Promise.all(statusPromise);
//     next();
// });

// mainSchema.pre(/^find/, function (next){
//     this.populate({
//         path: 'status',
//         select: '-__v'
//     })
//
//     next();
// });

const Main = mongoose.model('main', mainSchema);

module.exports = {
    Main,
    mainSchema
};
