const mongoose = require('mongoose');
const {itemLineageNodeSchema} = require("./itemLineageNode");


const proportionedFromNodeSchema = new mongoose.Schema({

    itemLineageNode: [itemLineageNodeSchema]
});

const proportionedFromNode = mongoose.model('proportionedFromNode', proportionedFromNodeSchema);

module.exports = {
    proportionedFromNode,
    proportionedFromNodeSchema
}