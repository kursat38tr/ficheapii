const mongoose = require('mongoose');
const {preparationDescriptionTranslationSchema} = require("./preparationDescriptionTranslation");
const {mediaSchema} = require("./media");

const preparationSchema = new mongoose.Schema({
    id: {
        type: Number,
        required: [true, 'Please tell us the id']
    },
    type: {
        type: String,
        required: [true, 'Please tell us the id']
    }
});

const preparation = mongoose.model('preparation', preparationSchema);

module.exports = {
    preparation,
    preparationSchema
}