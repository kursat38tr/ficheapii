const mongoose = require('mongoose');
const {salesPriceSchema} = require("./salesPrice");

const pricingAndQuantitySchema = new mongoose.Schema({
    pricingId: {
        type: Number,
        required: [true, 'Please tell us the id']
    },
    itemId: {
        type: Number,
        required: [true, 'Please tell us the id']
    },
    type: {
        type: String,
        required: [true, 'Please tell us the id']
    },
    amount: {
        type: Number,
        required: [true, 'Please tell us the id']
    },
    unitOfMeasurement: {
        type: String,
        required: [true, 'Please tell us the id']
    },
    priceExclVat: {
        type: Number,
        required: [true, 'Please tell us the id']
    },
    priceInclVat: {
        type: Number,
        required: [true, 'Please tell us the id']
    },
    VatType: {
        type: String,
        required: [true, 'Please tell us the id']
    },
    vatAmount: {
        type: Number,
        required: [true, 'Please tell us the id']
    },
    measuredWeightInGrams: {
        type: Number,
        required: [true, 'Please tell us the id']
    },
    pricePerMeasuredGram: {
        type: Number,
        required: [true, 'Please tell us the id']
    },
    salesPrice: [salesPriceSchema],
    validFrom: {
        type: Date,
        required: [true, 'Please tell us the id']
    },
    validTo: {
        type: Date,
        required: [true, 'Please tell us the id']
    },
    packingType: {
        type: String,
        required: [true, 'Please tell us the id']
    },
});

const pricingAndQuantity = mongoose.model('pricingAndQuantity', pricingAndQuantitySchema);

module.exports = {
    pricingAndQuantity,
    pricingAndQuantitySchema
}