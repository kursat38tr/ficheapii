const mongoose = require('mongoose');
const {marketingNameTranslationSchema} = require("./marketingNameTranslation");


const salesDataSchema = new mongoose.Schema({
    marketingName: {
        type: String,
        required: [true, 'Please tell us the marketingName']
    },
    marketingNameTranslation: [marketingNameTranslationSchema],
    registerKey: {
        type: String,
        required: [true, 'Please tell us the registerKey']
    },
});

const salesData = mongoose.model('salesData', salesDataSchema);

module.exports = {
    salesData,
    salesDataSchema
}