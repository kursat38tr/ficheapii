const mongoose = require('mongoose');

const allergenSchema = new mongoose.Schema({
    itemAllergensId: {
        type: Number,
        required: [true, 'Please tell us the id']
    },
    itemAllergensItemId: {
        type: Number,
        required: [true, 'Please tell us the id']
    },
    itemAllergensAllergenId: {
        type: String,
        required: [true, 'Please tell us the id']
    },
    itemAllergensNameNl: {
        type: String,
        required: [true, 'Please tell us the id']
    },
    itemAllergensNameGb: {
        type: String,
        required: [true, 'Please tell us the id']
    },
    itemAllergensPresent: {
        type: Boolean,
        required: [true, 'Please tell us the id']
    },
});

const allergen = mongoose.model('allergen', allergenSchema);

module.exports = {
    allergen,
    allergenSchema
}