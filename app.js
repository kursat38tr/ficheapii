const mongoose = require('mongoose');
const express = require('express');
const bodyParser = require('body-parser');

const itemRouter = require('./routes/itemRoutes');
const mainRouter = require('./routes/mainRoutes');
const statusRoute = require('./routes/statusRoute');

const _db = 'mongodb+srv://root:root123@ficheapi.mcvow.mongodb.net/dbtest?retryWrites=true&w=majority';

const app = express();


// accept url encoded
app.use(bodyParser.urlencoded({
    extended: true
}));

mongoose.connect(_db, {useNewUrlParser: true, useUnifiedTopology: true})
    .then((result) => console.log('connected'))
    .catch((error) => console.log(error));

app.use(express.json());
app.use(express.json({limit: '10kb'}));
app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({extended: true}))

const port = 3000;
app.listen(port, () =>{
    console.log(`App is running on ${port}`);
});

app.use('/status', statusRoute);
app.use('/item', itemRouter);
app.use('/main', mainRouter);



