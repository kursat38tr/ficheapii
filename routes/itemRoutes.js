const express = require('express');
const item = require('../controller/itemController');

const router = express.Router();

router.get("/getAllItems", item.getAllItems);
router.post("/create", item.createItem);
router.put("/update", item.updateItem);

module.exports = router;